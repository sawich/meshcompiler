// c++
#include <iostream>
#include <fstream>
#include <sstream>
#include <filesystem>

// ea
#include <include/console.hpp>

namespace message
{
	int32_t example (void) noexcept
	{
		//
		ea::console::color::set (240);
		std::cout << "MeshCompiler.exe output_file_path";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in current directory\n\n";

		//
		ea::console::color::set (240);
		std::cout << "MeshCompiler.exe output_file_path -dir directory_name";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in specified directory\n\n";

		//
		ea::console::color::set (240);
		std::cout << "MeshCompiler.exe output_file_path -sub";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in current directory with subdirectories\n\n";

		//
		ea::console::color::set (240);
		std::cout << "MeshCompiler.exe output_file_path -sub -dir directory_name";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << "\npack all files in specified directory with subdirectories\n\n";

		return EXIT_SUCCESS;
	} // int32_t example (void) noexcept

	template <typename Path>
	int32_t cant_create (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't created";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t cant_create (Path const& _path) noexcept

	template <typename Path>
	int32_t cant_create_dir (Path const& _path) noexcept
	{
		std::cout << "dir ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't created";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t cant_create_dir (Path const& _path) noexcept

	template <typename Path>
	int32_t no_exist (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "not found";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t no_exist (Path const& _path) noexcept

	template <typename Path>
	int32_t cant_read (Path const& _path) noexcept
	{
		std::cout << "file ";

		ea::console::color::set (240);
		std::cout << _path;

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << " ";

		ea::console::color::set (192);
		std::cout << "can't be read";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		return EXIT_FAILURE;
	} // int32_t cant_read (Path const& _path) noexcept

	template <typename Path>
	void ok (Path const& _path) noexcept
	{
		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << ' ' << _path << ' ';

		ea::console::color::set (240);
		std::cout << "ok";

		ea::console::color::set (ea::console::color::value::DARKGRAY);
		std::cout << '\n';
	} // void ok (uint32_t const _offset, std::string const& _path) noexcept
} // namespace message

class pack
{
	class packer {
	public:
		template <typename Iterator>
		int32_t pack (Iterator _iterator, std::filesystem::path _output_path) noexcept
		{
			if (!std::filesystem::is_directory (_output_path) && !std::filesystem::create_directories(_output_path)) {
				message::cant_create_dir (_output_path);
				return EXIT_FAILURE;
			}
			
			for (auto const& entry : _iterator) {
				const auto path{ entry.path () };
				if (!entry.is_regular_file () || path.extension () != ".obj") { continue; }

				this->process_raw_file (path, _output_path);
			}

			return EXIT_SUCCESS;
		}

	private:
		std::string read_raw (std::filesystem::path const& _path) noexcept
		{
			std::string file_raw;

			std::ifstream file{ _path };
			if (!file) {
				message::cant_read (_path);
				return file_raw;
			}

			file.seekg (0, std::ios::end);
			file_raw.reserve (file.tellg ());
			file.seekg (0, std::ios::beg);

			file_raw.assign ((std::istreambuf_iterator <char> (file)), std::istreambuf_iterator<char> ());

			return file_raw;
		}

		struct V  { float x, y, z; };
		struct T { float u, v; };
		struct N { float x, y, z; };
		using F = std::array <std::array <uint64_t, 3>, 3>;

		struct raw_mesh_fragment_t
		{
			std::vector <F> f;
			std::filesystem::path filename;
		};

		struct raw_mesh_t
		{
			std::vector <V> v;
			std::vector <T> vt;
			std::vector <N> vn;
			std::vector <raw_mesh_fragment_t> fragments;
		};

		raw_mesh_t parse_raw (std::string const& raw) noexcept
		{
			raw_mesh_t mesh;
			raw_mesh_fragment_t current;

			std::istringstream buffer{ raw };
			std::string line;
			while (buffer >> line)
			{
				if ("o" == line)
				{
					if ("" != current.filename)
					{
						mesh.fragments.emplace_back (current);
						current = raw_mesh_fragment_t{};
					}
					buffer >> current.filename;
					continue;
				}
				if ("v" == line)
				{
					V v;
					buffer >> v.x >> v.y >> v.z;
					mesh.v.emplace_back (v);
					continue;
				}
				if ("vt" == line)
				{
					T vt;
					buffer >> vt.u >> vt.v;
					mesh.vt.emplace_back (vt);
					continue;
				}
				if ("vn" == line)
				{
					N vn;
					buffer >> vn.x >> vn.y >> vn.z;
					mesh.vn.emplace_back (vn);
					continue;
				}
				if ("f" == line)
				{
					F f;
					for (size_t q{ 0 }; q != 3; ++q)
					{
						buffer >> f[q][0];
						buffer.get ();
						buffer >> f[q][1];
						buffer.get ();
						buffer >> f[q][2];
					}
					current.f.emplace_back (f);
					continue;
				}

				std::getline (buffer, line, '\n');
			}

			if ("" != current.filename)
				mesh.fragments.emplace_back (current);

			return mesh;
		}

		struct compiled_mesh_point_t { V v; T t; N n; };
		
		void compile_and_save (raw_mesh_t const& mesh, std::filesystem::path const& _output_path) noexcept
		{
			for (const auto& raw : mesh.fragments)
			{
				const auto out{ _output_path / raw.filename.filename ().stem () };
				std::ofstream file{ out };
				if (file.fail ()) {
					message::cant_create (out);
					continue;
				}

				std::vector <compiled_mesh_point_t> compiled;

				// 3 -- triangle 
				compiled.reserve (std::size (raw.f) * 3);

				for (const auto& f : raw.f)
					for (const auto& row : f)
						compiled.emplace_back (compiled_mesh_point_t{
							{ mesh.v[row[0] - 1] },
							{ mesh.vt[row[1] - 1] },
							{ mesh.vn[row[2] - 1] },
						});

				file.write (
					reinterpret_cast <const char*> (std::data (compiled)),
					std::size (compiled) * sizeof (compiled_mesh_point_t));

				message::ok (std::filesystem::absolute(out));
			}
		}

		void process_raw_file (std::filesystem::path const& _path, std::filesystem::path const& _output_path) noexcept
		{
			// archive file
			auto const file_raw{ this->read_raw (_path) };
			auto parsed_raw{ parse_raw (file_raw) };
			compile_and_save (parsed_raw, _output_path);
		}
	};

	template <typename Path, typename Iterator>
	static int32_t _pack (Path const _output_path, Iterator _iterator) noexcept
	{
		packer packer{};
		return packer.pack (_iterator, std::filesystem::path{ _output_path });
	} // static int32_t _pack (int32_t const argc, char const* argv[]) noexcept
public:
	static int32_t all (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (std::filesystem::path{ std::filesystem::current_path () / "out" }, std::filesystem::directory_iterator (std::filesystem::current_path ()));
	} // static int32_t all (int32_t const argc, char const* argv[]) noexcept

	static int32_t all_from_folder (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::directory_iterator (std::filesystem::path (argv[3])));
	} // static int32_t all_from_folder (int32_t const argc, char const* argv[]) noexcept

	static int32_t recursive (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::recursive_directory_iterator (std::filesystem::current_path ()));
	} // static int32_t recursive (int32_t const argc, char const* argv[]) noexcept

	static int32_t recursive_from_folder (int32_t const argc, char const* argv[]) noexcept
	{
		return pack::_pack (argv[1], std::filesystem::recursive_directory_iterator (std::filesystem::path (argv[4])));
	} // static int32_t recursive (int32_t const argc, char const* argv[]) noexcept
}; // class pack

int32_t main (int32_t const argc, char const* argv[]) noexcept
{
	// pack all files in current directory
	if (2 == argc) { return pack::all (argc, argv); }
	// pack all files in specified directory
	else if (4 == argc && 0 == strcmp ("-dir", argv[2])) { return pack::all_from_folder (argc, argv); }
	// pack all files in current directory with subdirectories
	else if (3 == argc && 0 == strcmp ("-sub", argv[2])) { return pack::recursive (argc, argv); }
	// pack all files in specified directory with subdirectories
	else if (5 == argc && 0 == strcmp ("-sub", argv[2]) && 0 == strcmp ("-dir", argv[3])) { return pack::recursive_from_folder (argc, argv); }

	return message::example ();
} // int32_t main (int32_t const argc, char const* argv[]) noexcept